import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Tracker } from 'meteor/tracker';

import { getTweets } from '../api/twitter.js';
import { addTweetMarkers } from '../api/googlemaps.js';
import './tweet.js';
import './tweet.html';
import './body.html';

var error = function(err, response, body) {
  console.log('ERROR [%s]', err);
};

var tweets = new ReactiveVar(0);

Template.body.onCreated(function() {
  Tracker.autorun(function() {
    GoogleMaps.ready('map', function(map) {
      addTweetMarkers(map, tweets.get());
    });      
  });
  Meteor.call('getTweets', (err, res) => {
	  console.log(res);
    tweets.set(res);   
  });
});

Template.body.helpers({
  mapOptions() {
    if (GoogleMaps.loaded()) {
      return {
        center: new google.maps.LatLng(27.6443354, -11.0121044),
        zoom: 2
      };
    }
  },
  tweets() {
    return tweets.get();
  },
	hasTweets() {
	  return tweets.get() && tweets.get().length > 0;
	}
})

Template.body.onRendered(function() {
  GoogleMaps.load({ key: 'AIzaSyCQEUzLjoQOMOfgFTwQNglAPKoYYRBXVRI'});
});

Template.body.events({
  'submit .search'(event) {
    // Prevent default browser form submit
    event.preventDefault();
 
    // Get value from form element
    const target = event.target;
    const text = target.text.value;
 
	  Meteor.call('getTweets', text, (err, res) => {
		  console.log(res);
	    tweets.set(res); 
      addTweetMarkers(GoogleMaps.maps.map, res);  
	  });
  },
});
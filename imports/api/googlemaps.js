import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';

const apiKey = 'AIzaSyCQEUzLjoQOMOfgFTwQNglAPKoYYRBXVRI';
const apiKey2 = 'AIzaSyBuDUk2OLJJbEfsjjkyHj74J5473BQD-P0';

var markers = [];
var prevInfoWindow = false;

function clearTweetMarkers() {
  markers.forEach(function(marker) {
    marker.setMap(null);
  });
  markers = [];
} 

function addTweetMarkers(map, tweets) {
  clearTweetMarkers();
  if (tweets && tweets.length > 0) {
    tweets.forEach(function(tweet) {
      if (tweet.user && tweet.user.location) {
        Meteor.call('lookupLocation', tweet.user.location, (err, res) => {
          if (res) {
            var contentString = '<div id="content">' +
              '<div class="text-success">@' + tweet.user.screen_name + ': </div>' +
              '<div>' + tweet.full_text + '</div> </div>';

            var infowindow = new google.maps.InfoWindow({
              content: contentString
            }); 
            var marker = new google.maps.Marker({
              position: res,
              map: map.instance
            });
            
            marker.addListener('click', function() {
              if (prevInfoWindow) {
                prevInfoWindow.close();
              }
              prevInfoWindow = infowindow;
              infowindow.open(map, marker);
            });
            markers.push(marker);
          }
        });
      }
    });
  }
}

function lookupLocation(loc) {
  try {
    var result = HTTP.call('GET', 'https://maps.googleapis.com/maps/api/geocode/json', {
      params: { key: apiKey2, address: loc }
    });
    
    var json = JSON.parse(result.content);
    if (json.results.length > 0) {
      return json.results[0].geometry.location;
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

export { addTweetMarkers, lookupLocation };
## Mashup-App

Idee der Hausübung ist, eine WebApp zu entwickeln, die mindestens zwei REST APIs abfragt und diese miteinander "masht". Dabei soll die App mit Meteor geschrieben, mit Bootstrap dargestellt und auf Heroku gehostet werden.

Diese WebApp verwendet drei APIs, einerseits von Twitter und andererseits von GoogleMaps und Google Geocoding. Dabei wird die Twitter API verwendet, um möglichst aktuelle Tweets abzuholen über ein spezifisches Thema. Dieses Thema wird mittels Search-Bar vom User selber ausgewählt. Anschließend werden diese Tweets auf einer, mit der GoogleMaps API dargestellten Map mit einem Marker angezeigt. Hierfür wird der Ort aus der JSON-Datei der Tweets ausgelesen, mit der Geocoding API in passende Koordinaten umgewandelt und so auf der Karte mit einem Marker gekennzeichnet. Sind die Tweet-Marker nun auf der Karte zu sehen, können diese angeklickt werden und man erhält den Usernamen und den Tweet-Inhalt in einer pop-up Textarea. Zusätzlich findet man noch ein Twitter-feed unterhalb der Karte, wo alle gefundene Tweets angesehen werden können. Aktuell werden maximal 50 Tweets angezeigt. Das Tab ”Info” innerhalb der WebApp enthält ein drop-down mit Name und Matrikelnummer.

## Installationsschritte

Die WebApp kann lokal auf dem eigenen Rechner laufengelassen werden. Beachten Sie, dass die App lokal mit Meteor gestartet wird, das heißt Meteor muss vorher schon installiert sein. Wenn sie Meteor schon haben, dann können folgende Schritte ausgeführt werden:

    1. Das Projekt klonen
    2. In das geklonte Hauptverzeichnis wechseln
    3. Innerhalb des Hauptverzeichnisses `meteor run` laufen lassen
    
Damit sollte die Webapp nun lokal auf ihrem System laufen unter localhost:3000

### Hinweise zur Evaluation mit Lighthouse

Wenn der Test zur Barrierefreiheit mit Lighthouse durchgeführt wird, kommt man auf ein Wert von 92. Das liegt unter Anderem daran, dass die von Google Maps dargestellten Karte intern Buttons besitzt zum Vergrößern oder Verkleinern. Diese Buttons haben keine Alternative-Bezeichnung, weswegen Lighthouse Punkte abzieht. Die Website ist aber, vom Farbdesign her, barrierefrei und hat auch, nach dem Test mit Wave, keine Kontrastfehler.

### Hinweise zur Abfrage der API Keys

Die Abfrage der Tweets erfolgt über dem Package "Twit". Aus diesem Grund ist es schwer möglich die Abfrage der API von Twitter über Postman per Hand zu machen. Auch die verwendete API von Google Maps ist nur schwer testbar mit Postman, da diese nur für das Anzeigen der Google Maps Karte verwendet wird. Der Test zur Google Geolocation ist jedoch vorhanden.
import { Meteor } from 'meteor/meteor';
import { getTweets } from '../imports/api/twitter.js';
import { lookupLocation } from '../imports/api/googlemaps.js';

Meteor.startup(() => {
	Meteor.methods({
	  getTweets: getTweets,
		lookupLocation: lookupLocation
	});
});
